<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title','Welldone!App') </title>
     {{-- icon --}}
     <link rel="icon" href="{{ asset('favicon.png') }}">




    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>
<body>
    <div id="app">
        {{--  header  --}}
        @include('app.header')

        <main id="app-main">
            @yield('content')
        </main>

        @include("app.footer")



    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
		@yield('scripts')
</body>
</html>
