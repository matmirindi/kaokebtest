<div class="m-footer">
    <div class="f--wrapper">
        <div class="apps-container">
            <div class="apps-wrapper">
                 <div class="caption">Get the Welldone!App</div>
                <div class="img-container">
                    <ul class="list-inline">
                        <li class="list-inline-item">  <a href="">
                            <img src="{{ asset('images/app/app-store.svg') }}" alt="">
                        </a> </li>
                        <li class="list-inline-item">  <a href="">
                                <img src="{{ asset('images/app/google-play-badge-01.png') }}">
                        </a> </li>
                    </ul>
                </div>
            </div>


        </div>
        <div class="credits-c">
            <div class="c--wrapper">
                <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="" class="linkedin"><i class="fab fa-linkedin-in    "></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="" class="twitter"><i class="fab fa-twitter    "></i></a>
                        </li>
                        <li class="list-inline-item">
                            <span>Welldone!App - 2018
                            </span>
                        </li>
                    </ul>
            </div>

            </div>
    </div>
</div>
