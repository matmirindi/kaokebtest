@extends('layouts.master')

@section('content')

    <div class="ap-bx-p">
        {{--  hiro  --}}
        <section class="hiro">
            <div class="wrapper">
                <div>
                    <div class="main-title">Your TEAM DESERVE</div>
                    <div class="second-title">recognition</div>
                </div>
                <div class="box-container">
                    <div class="b-title">Because recognition is...</div>
                    <ul>
                        <li>Crucial for your people’s <span>motivation</span>  and <span>well being</span> </li>
                        <li>A key driver of your <span>organization's performance</span> </li>
                        {{--  '  --}}
                        <li><span>Too often forgotten</span>  by supervisors</li>
                    </ul>
                </div>

            </div>
        </section>
        {{--  make it happen  --}}
        <section class="m-happen-c">
            <div class="s-wrapper">
                <div class="title-c"><h1 class="title">Make it happen <br> in three simple steps
                    </h1>
                </div>
                <div class="s-bdy">
                    {{--    --}}
                    <div class="option-c">
                        <div class="img-container">
                            <div class="img-wrapper"><img src="{{ asset('images/illu-01.svg') }}" alt=""></div>
                        </div>
                        <div class="option-t">Remember</div>
                        <div class="option-caption">Every positive achievement
                                <br> with <b> Welldone!App</b></div>
                        <div class="option-more">
                            <div class="toggler" id="toggler-1"><a href="" onclick="event.preventDefault();showMore('more-1','toggler-1');"><i class="fas fa-plus    "></i></a></div>
                            <div class="more-container not-visible" id="more-1">
                                <div class="more-c ">
                                <ul>
                                    <li><b>Take a note </b>any event, anytime, anywhere</li>
                                    <li><b>Never forget </b> a single achievement</li>
                                    <li><b>Relieve your mind :</b> Welldone!App will remember for you</li>
                                </ul>
                                <a href="" class="closer" onclick="event.preventDefault();hideMore('more-1','toggler-1');"><i class="fas fa-times    "></i></a>
                            </div></div>

                        </div>
                    </div>
                    {{--    --}}
                    <div class="option-c">
                        <div class="img-container">
                            <div class="img-wrapper"><img src="{{ asset('images/illu-02.svg') }}" alt=""></div>
                        </div>
                        <div class="option-t">Include</div>
                        <div class="option-caption">Recognition into your interviews
                              <br>  with <b> Welldone!App Feedbacks</b></div>
                        <div class="option-more">
                            <div class="toggler" id="toggler-2"><a href="" onclick="event.preventDefault();showMore('more-2','toggler-2');"><i class="fas fa-plus    "></i></a></div>
                            <div class="more-container not-visible" id="more-2">
                                <div class="more-c ">
                                <ul>
                                    <li><b>Build</b> structured feedbacks</li>
                                    <li><b>Illustrate</b> every statement with facts</li>
                                    <li>Save preparation time and make your feedbacks <b>more impactful</b></li>
                                </ul>
                                <a href="" class="closer" onclick="event.preventDefault();hideMore('more-2','toggler-2');"><i class="fas fa-times    "></i></a>
                            </div></div>

                        </div>
                    </div>
                    {{--    --}}
                    <div class="option-c">
                        <div class="img-container">
                            <div class="img-wrapper"><img src="{{ asset('images/illu-03.svg') }}" alt=""></div>
                        </div>
                        <div class="option-t">Escalate</div>
                        <div class="option-caption">Every positive achievement
                                <br> with <b> Welldone!App</b></div>
                        <div class="option-more">
                            <div class="toggler" id="toggler-3"><a href="" onclick="event.preventDefault();showMore('more-3','toggler-3');"><i class="fas fa-plus    "></i></a></div>
                            <div class="more-container not-visible" id="more-3">
                                <div class="more-c ">
                                <ul>
                                    <li>Select <b>your team’s best achievements </b>and send them to your top leaders</li>
                                    <li><b>Request </b> them to give
                                      <b> meaningful feedback</b>  to your
                                        team members</li>
                                    <li><b>Enjoy </b> people’s smile & renewed motivation</li>
                                </ul>
                                <a href="" class="closer" onclick="event.preventDefault();hideMore('more-3','toggler-3');"><i class="fas fa-times    "></i></a>
                            </div></div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{--  join community  --}}
        <section class="community-p">
            <div class="wrapper">
                <div class="caption-c"><h3 class="caption">Join the community of managers for free</h3></div>
                <div class="actions-container">
                    <div><a href="" class="p-btn orange-bg">Join us free</a></div>
                    <div><a href="" class="p-btn">Login</a></div>
                </div>
            </div>
        </section>
        {{--  contact  --}}
        <section class="contact-form">
            <div class="wrapper">
                {{--  caption  --}}
                <div class="caption-container">
                    <h3 class="c-title">
                            Discover <br>Welldone!App <br>Business
                    </h3>
                    <ul>
                        <li>Ensure a robust day-to-day Recognition process across your organization</li>
                        <li>Centralize accounts & user administration</li>
                        <li>Access all activity history</li>
                        <li>Obtain KPI’s, dashboards & reports</li>
                    </ul>
                </div>
                {{--  form  --}}
                <div class="form-container">
                    <div class="form-wrapper">
                        <form action="">
                            <div class="form-group">
                                <label for="" class="required">Company</label>
                                <input type="text" name="company" id="" >
                            </div>
                            <div class="form-group">
                                <label for="" class="required">Name</label>
                                <input type="text" name="name" id="" >
                            </div>
                            <div class="form-group">
                                <label for="" class="required">First Name</label>
                                <input type="text" name="first-name" id="" >
                            </div>
                            <div class="form-group">
                                <label for="" class="required">Professional email*</label>
                                <input type="email" name="email" id="" >
                            </div>
                            <div class="form-group">
                                <textarea name="content" id="" placeholder="How can we help you?*"></textarea>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="p-btn orange-bg">Send me offers !</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>



@endsection
@section('scripts')
<script>

        function showMore(id,toggler)
        {
            var more =   document.getElementById(id);
            var t = document.getElementById(toggler);
            more.classList.remove('not-visible');
            t.style.opacity = 0;
        }
        function hideMore(id,toggler)
        {
            var more =   document.getElementById(id);
            var t = document.getElementById(toggler);
            more.classList.add('not-visible');
            t.style.opacity = 1;
        }
        function toggleSmallMenu()
        {
            var x = document.getElementById("small-menu");
            var img = document.getElementById("indc");
            var g = document.getElementById("mh-overlay")
            if (x.classList.contains("not-visible")) {
                x.classList.remove("not-visible")
                img.src = '/images/groupe-84.svg';
                g.style.display = "block";
            } else {
                x.classList.add("not-visible");
                img.src = '/images/grille-de-r-p-tition-10.svg';
                g.style.display = "none";
            }
        }

</script>
@endsection
