
<div class="master-hdr">
    <div class="h--wrapper">
        <div class="logo-container">
            <a href="">Welldone!App</a>
        </div>
        <div class="menus-container">
            <div class="menu  menu-toggler"><a href="" class="" onclick="event.preventDefault();toggleSmallMenu()" ><img src="{{ asset('images/grille-de-r-p-tition-10.svg') }}" alt="" id="indc"></a></div>
            <div class="menu menu-option"><a href="" class="welcome">Welldone!App Business</a></div>
            <div class="menu  menu-option"><a href="" class="join p-btn orange-bg">Join us free</a></div>
            <div class="menu  menu-option"><a href="" class="login p-btn">Login</a></div>
            <div class="menu  menu-option"><a href="" class="">English <i class="fas fa-chevron-down    "></i></a></div>

        </div>

    </div>
    <div class="small-menu-container not-visible" id="small-menu">
        <div class="wrapper">
            <div class="menu menu-option"><a href="" class="welcome">Welldone!App Business</a></div>
            <div class="menu  menu-option"><a href="" class="join p-btn orange-bg">Join us free</a></div>
            <div class="menu  menu-option"><a href="" class="login p-btn">Login</a></div>
            <div class="menu  menu-option"><a href="" class="">English <i class="fas fa-chevron-down    "></i></a></div>
        </div>

    </div>
</div>
<div id="mh-overlay"></div>

